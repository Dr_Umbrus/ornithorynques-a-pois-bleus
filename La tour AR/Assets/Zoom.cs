﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zoom : MonoBehaviour
{
    private bool _zoom = false;
    public float _zoomSpeed = 5f;
    public GameObject _objet;
    public GameObject _zoomPoint;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0)
        {
            Debug.Log("Spawn");

            if (Input.GetTouch(0).phase == TouchPhase.Began)
            {
                if (_objet.activeSelf == true)
                {
                    _zoom = true;
                }
            }
        }

        if (_zoom == true)
        {
            _objet.gameObject.transform.Translate(_zoomPoint.transform.position * _zoomSpeed * Time.deltaTime);
        }
    }
}
