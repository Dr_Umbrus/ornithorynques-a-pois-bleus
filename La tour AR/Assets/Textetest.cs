﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Textetest : MonoBehaviour
{
    public GameObject _text;
    public GameObject _Bot;

    // Start is called before the first frame update
    void Start()
    {
        
        _text = GameObject.Find("Description");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0)
        {
            Debug.Log("ok");

            if (Input.GetTouch(0).phase == TouchPhase.Began)
            {
                if(_text.activeSelf == false)
                {
                    _text.SetActive(true);
                }
                else
                {
                    _text.SetActive(false);
                }
            }

        }
    }
}
