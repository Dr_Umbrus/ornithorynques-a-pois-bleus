﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn_Object : MonoBehaviour
{
    public GameObject _perso;
    public GameObject _objet1;
    public GameObject _objet2;

    // Start is called before the first frame update
    void Start()
    {
        _perso = GameObject.Find("perso");
        _objet1 = GameObject.Find("Objet 1");
        _objet2 = GameObject.Find("Objet 2");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0)
        {
            Debug.Log("Spawn");

            if (Input.GetTouch(0).phase == TouchPhase.Began)
            {
                if (_perso.activeSelf == true)
                {
                    _objet1.SetActive(true);
                    _objet2.SetActive(true);                  
                }
            }
        }
    }
}

