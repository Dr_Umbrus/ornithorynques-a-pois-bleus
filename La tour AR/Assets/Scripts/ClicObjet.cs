﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClicObjet : MonoBehaviour
{
    public GameObject UIinfo;
    //public GameObject SoundManager;

    public int _objet;
    public int _son;

    private SoundManagerScript SManager;

    // Start is called before the first frame update
    void Start()
    {
        SManager = FindObjectOfType<SoundManagerScript>();
    }

    // Update is called once per frame
    void Update()
    {
        // Clic sur un objet

        if (Input.GetMouseButtonDown(0))                                                            // Esteban - A l'input d'un clic de souris.
        {
            RaycastHit hit;                                                                         // Esteban - Création d'un RayCast.
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);                            // Esteban - Utilisation du RayCast afin de vérifier si je clique sur un objet.

            if (Physics.Raycast(ray, out hit, 1000.0f))                                             // Esteban - Si le RayCast trouve un objet
            {
                if (hit.transform.tag == "Objet")                                                   // Esteban - Et que cet objet a le tag "objet"
                {
                    UIinfo.SetActive(true);                                                         // Esteban - Alors j'active le boutton d'infos qui était invisible jusque là
                    _son = hit.transform.gameObject.GetComponent<ClicObjet>()._objet;               // Esteban - La variable _Son prend comme valeur celle de l'objet sur lequel nous avons cliqué
                }
            }
        }
    }

    public void ClicInfos()
    {
        SManager.InfosDialogue(_son);                                                               // Esteban - Je lance la fonction InfosDialogue du script SoundManager
    }

    public void QuitObject()
    {
        UIinfo.SetActive(false);                                                                    // Esteban - Bouton retour qui permet de désactiver tout l'UI d'infos de l'objet
    }
}
