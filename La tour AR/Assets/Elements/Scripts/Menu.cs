﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    // FONCTION StartGame (Boutton Start)

    public void StartGame()
    {
        SceneManager.LoadScene("Antoine");                                                             // Esteban - Chargement de la scène de jeu.
    }

    //................................................................................................................................

    // FONCTION QuitGame (Boutton Quitter)

    public void QuitGame()
    {
        Application.Quit();                                                                         // Esteban - L'application se ferme.
    }
}
